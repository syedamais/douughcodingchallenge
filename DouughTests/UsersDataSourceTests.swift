//
//  UsersDataSourceTests.swift
//  DouughTests
//
//  Created by SH-Syed on 25/11/18.
//  Copyright © 2018 Skilledhealth. All rights reserved.
//

import XCTest
@testable import Douugh

class UsersDataSourceTests: XCTestCase {
    
    var dataSource : UsersDataSource!
    
    override func setUp() {
        super.setUp()
        dataSource = UsersDataSource()
    }
    
    override func tearDown() {
        dataSource = nil
        super.tearDown()
    }
    
    func testEmptyValueInDataSource() {
        
        // giving empty data value
        dataSource.userData.value = []

        let tableView = UITableView()
        tableView.dataSource = dataSource
        
        // expected 1 section
        XCTAssertEqual(dataSource.numberOfSections(in: tableView), 1, "Expected 1 section in table view")
        
        // expected zero cell for users data
        XCTAssertEqual(dataSource.tableView(tableView, numberOfRowsInSection: 0), 0, "Expected 0 cell in table view")
        
    }
    
    func testUsersDataInDataSource() {
        
        // giving data value
        guard let data = FileManager.readJson(forResource: "sample") else {
            XCTAssert(false, "Can't get data from sample.json")
            return
        }
        
        guard let users = try? JSONDecoder().decode([User].self, from: data) else {
            XCTAssert(false, "Parsing error")
            return
        }
        
        dataSource.userData.value = users
        
        let tableView = UITableView()
        tableView.dataSource = dataSource
        
        // expected one section //
        XCTAssertEqual(dataSource.numberOfSections(in: tableView), 1, "Expected one section in table view")
        
        // expected 2 cell for users data //
        XCTAssertEqual(dataSource.tableView(tableView, numberOfRowsInSection: 0), 2, "Expected two cell in table view")
    }
    
}
