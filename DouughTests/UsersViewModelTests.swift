//
//  UsersViewModelTests.swift
//  DouughTests
//
//  Created by SH-Syed on 25/11/18.
//  Copyright © 2018 Skilledhealth. All rights reserved.
//

import XCTest
@testable import Douugh

class UsersViewModelTests: XCTestCase {
    
    var viewModel : UsersViewModel!
    let dataSource = UsersDataSource()
    
    override func setUp() {
        super.setUp()
        self.viewModel = UsersViewModel(dataSource: dataSource)
    }
    
    override func tearDown() {
        self.viewModel = nil
        super.tearDown()
    }
    
    
    func testFetchUsersData() {
        
        // fetching user data //
        viewModel.getUsers(){ result in
            switch result {
            case .success(_) :
                XCTAssert(false, "ViewModel should not able to fetch users data")
            default:
                break
            }
        }
    }

}

