//
//  UserTests.swift
//  DouughTests
//
//  Created by SH-Syed on 25/11/18.
//  Copyright © 2018 Skilledhealth. All rights reserved.
//

import XCTest
@testable import Douugh

class UserTests: XCTestCase {


    func testInitialization() {
        guard let data = FileManager.readJson(forResource: "sample") else {
            XCTAssert(false, "Can't get data from sample.json")
            return
        }
        
        guard let users = try? JSONDecoder().decode([User].self, from: data) else {
            XCTAssert(false, "Parsing error")
            return
        }

        let user = users.first

        XCTAssertEqual("Bob", user?.firstName)
        XCTAssertEqual("Ong", user?.lastName)
        XCTAssertEqual("bob@ong.com", user?.email)
        XCTAssertEqual(1, user?.id)
    }
    
    func testParseEmptyResponse() {
        
        let data = Data()

        
        guard (try? JSONDecoder().decode([User].self, from: data)) != nil else {
            XCTAssert(true, "Expected failure when no data")
            return
        }

    }


}

extension FileManager {

    static func readJson(forResource fileName: String ) -> Data? {

        let bundle = Bundle(for: DouughTests.self)
        if let path = bundle.path(forResource: fileName, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                return data
            } catch {
                // handle error
            }
        }

        return nil
    }
}
