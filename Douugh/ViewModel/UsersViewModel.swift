//
//  UsersViewModel.swift
//  Douugh
//
//  Created by SH-Syed on 25/11/18.
//  Copyright © 2018 Skilledhealth. All rights reserved.
//

import Foundation

class UsersViewModel {
    
    var repository: UsersRepsository?
    weak var dataSource : GenericDataSource<User>?
    
    
    init(dataSource : GenericDataSource<User>?) {
        self.dataSource = dataSource
        repository = UsersRepsository()
    }
    
    func getUsers(completion: ((ItemDataResponse) -> Void)? = nil) {
        guard let repo = repository else { return }
        
        repo.getUser() { [weak self](response) in
            guard let strongSelf = self else { return }
            
            switch response {
            case .success(let result):
                strongSelf.dataSource?.userData.value = result
                completion?(.success(result: result))
            case.failure(let error):
                completion?(.failure(error: error))
                break
            }
        }
    }
    
}
