//
//  UsersDataSource.swift
//  Douugh
//
//  Created by SH-Syed on 25/11/18.
//  Copyright © 2018 Skilledhealth. All rights reserved.
//

import Foundation
import UIKit

class GenericDataSource<T> : NSObject {
    var userData: DynamicValue<[User]> = DynamicValue([])

}

class UsersDataSource : GenericDataSource<User>, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if userData.value.count > 0 {
                return userData.value.count
            
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath) as! UserCell
        let user = userData.value[indexPath.row]
        cell.configureUserCell(user: user)
        return cell;
    }
    
}
