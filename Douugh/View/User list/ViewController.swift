//
//  ViewController.swift
//  Douugh
//
//  Created by SH-Syed on 25/11/18.
//  Copyright © 2018 Skilledhealth. All rights reserved.
//

import UIKit

protocol UserListDelegate: class {
    func set(type: Int)
}

class ViewController: UIViewController {
    
    private var userTable: UITableView!
    private var userSelector : UISegmentedControl!
    let dataSource = UsersDataSource()
    
    // Setup ViewModel for users
    lazy var usersVM : UsersViewModel = {
        let usersVM = UsersViewModel(dataSource: dataSource)
        return usersVM
    }()
    
    // init refresh control
    var refreshControl: UIRefreshControl = {
        return UIRefreshControl()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup UI //
        setupUI()
        
        // load user data //
        loadUsersData()
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
}

// MARK: - Users Data

extension ViewController {
    
    func setupUI() {
        
        // adding table view //
        let displayWidth: CGFloat = self.view.frame.width
        
        userTable = UITableView()
        userTable.register(UserCell.self, forCellReuseIdentifier: "UserCell")
        userTable.estimatedRowHeight = 90
        userTable.rowHeight = UITableView.automaticDimension
        userTable.dataSource = self.dataSource
        userTable.delegate = self
        self.view.addSubview(userTable)
        
        // setting navigation title //
        self.title = "Douugh"
        
        // adding pull to refresh //
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh", attributes: [.foregroundColor: UIColor.white])
        refreshControl.backgroundColor = .white
        refreshControl.tintColor = .black
        refreshControl.addTarget(self, action: #selector(loadUsersData), for: .valueChanged)
        userTable.addSubview(refreshControl)
        
        // adding segmented control ///
        let items = ["First", "Last", "ID"]
        userSelector = UISegmentedControl(items: items)
        userSelector.selectedSegmentIndex = 0
        
        // Add this custom Segmented Control to our view
        self.view.addSubview(userSelector)
        
        // Set up Frame and SegmentedControl
        userSelector.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(50)
            make.width.equalTo(displayWidth - 100)
            make.top.equalTo(self.view.snp.top).offset(70)
            make.left.equalTo(self.view.snp.left).offset(50)
            
        }
        
        // Style the Segmented Control
        userSelector.backgroundColor = UIColor.black
        userSelector.tintColor = UIColor.white
        userSelector.layer.borderColor = UIColor.darkGray.cgColor
        userSelector.layer.borderWidth = 1.0
        userSelector.layer.cornerRadius = 10.0
        
        // Add target action method
        userSelector.addTarget(self, action: #selector(sortData(sender:)), for: .valueChanged)
        
        // setup table view frame //
        self.userTable.snp.makeConstraints { (make) -> Void in
            make.width.equalTo(self.view.snp.width)
            make.top.equalTo(self.userSelector.snp.bottom).offset(50)
            make.bottom.equalTo(self.view.snp.bottom)
        }
        
    }
    
    @objc func sortData(sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            self.dataSource.userData.value.sort(by: User.Comparison.sortByFirstName)
            self.userTable.reloadData()
            break
        case 1:
            self.dataSource.userData.value.sort(by: User.Comparison.sortByLastName)
            self.userTable.reloadData()
            break
        default:
            self.dataSource.userData.value.sort(by: User.Comparison.sortByID)
            self.userTable.reloadData()
            break
        }
    }
    
    
    @objc func loadUsersData() {
        
        usersVM.getUsers() { result in
            DispatchQueue.global().async {
                DispatchQueue.main.sync {
                    switch result {
                    case .success(let result):
                        self.dataSource.userData.value = result
                        self.sortData(sender: self.userSelector)
                        if self.refreshControl.isRefreshing {
                            self.refreshControl.endRefreshing()
                        }
                    case .failure( _): break
                        
                    }
                }
            }
        }
    }
}

// MARK: - Tableview delegate function

extension ViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        let userDetail = UserDetailView()
        let user = self.dataSource.userData.value[indexPath.row]
        userDetail.fullName.text = user.firstName! + " " + user.lastName!
        userDetail.email.text = user.email
        userDetail.userid.text = String(user.id)
        self.navigationController?.pushViewController(userDetail, animated: true)
        
    }
}
