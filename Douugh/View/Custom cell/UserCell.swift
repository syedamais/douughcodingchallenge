import UIKit
import SnapKit

class UserCell: UITableViewCell {
    
    var fullName = UILabel()
    var email = UILabel()
    
    enum VisualFormat: String {
        case HorizontalStackViewFormat = "H:|[stackView]|"
        case VerticalStackViewFormat = "V:|[stackView(>=50)]|"
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        // set table selection color
        let selectedView = UIView(frame: CGRect.zero)
        selectedView.backgroundColor = UIColor(red: 78/255, green: 82/255, blue: 93/255, alpha: 0.6)
        selectedBackgroundView  = selectedView
        
        fullName.lineBreakMode = .byWordWrapping
        fullName.numberOfLines = 0
        
        email.lineBreakMode = .byWordWrapping
        email.numberOfLines = 0

        let rowData = UIStackView()
        
        rowData.axis         = .vertical
        rowData.distribution = .fillEqually
        rowData.alignment = .fill
        rowData.spacing = 3.0
        rowData.translatesAutoresizingMaskIntoConstraints = false
        rowData.setContentCompressionResistancePriority(UILayoutPriority.required, for: .vertical)
        
        // add views for spacing
        rowData.addArrangedSubview(fullName)
        rowData.addArrangedSubview(email)
        
        contentView.addSubview(rowData)
        
        // setting frame according to stackview // 
        let viewsDictionary: [String:AnyObject] = ["stackView" : rowData]
        var newConstraints = [NSLayoutConstraint]()
        newConstraints += self.newConstraints(visualFormat: VisualFormat.HorizontalStackViewFormat.rawValue, viewsDictionary: viewsDictionary)
        newConstraints += self.newConstraints(visualFormat: VisualFormat.VerticalStackViewFormat.rawValue, viewsDictionary: viewsDictionary)
        contentView.addConstraints(newConstraints)
        self.updateConstraints()
        
    }
    
    private func newConstraints(visualFormat: String, viewsDictionary: [String:AnyObject]) -> [NSLayoutConstraint] {
        return NSLayoutConstraint.constraints(withVisualFormat: visualFormat, options: [], metrics: nil, views: viewsDictionary)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureUserCell(user: User) {
        
        // Configure the cell...
        self.fullName.text = user.firstName! + " " + user.lastName!
        self.email.text = user.email
        
    }
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        fullName.text  = nil
        email.text  = nil
    }
}
