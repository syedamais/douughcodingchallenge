//
//  UserDetailView.swift
//  Douugh
//
//  Created by SH-Syed on 28/11/18.
//  Copyright © 2018 Skilledhealth. All rights reserved.
//

import UIKit
import MapKit

class UserDetailView: UIViewController {
    
    // MARK: - Properties
    
    var fullName = UILabel()
    var email = UILabel()
    var userid = UILabel()
    
    var userDetail: UIStackView!

    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
}

extension UserDetailView {
    
    func setupUI() {
        
        self.view.backgroundColor = .white

        let barHeight: CGFloat = UIApplication.shared.statusBarFrame.size.height
        let displayWidth: CGFloat = self.view.frame.width
        
        userDetail = UIStackView()
        userDetail.translatesAutoresizingMaskIntoConstraints = false
        userDetail.axis = .vertical
        userDetail.alignment    = .leading
        userDetail.distribution = .equalSpacing
        self.view.addSubview(userDetail)
        
        fullName.adjustsFontSizeToFitWidth = true
        email.adjustsFontSizeToFitWidth = true
        userid.adjustsFontSizeToFitWidth = true

        userDetail.addArrangedSubview(fullName)
        userDetail.addArrangedSubview(email)
        userDetail.addArrangedSubview(userid)

        userDetail.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(150)
            make.width.equalTo(displayWidth - 100)
            make.left.equalTo(self.view.snp.left).offset(50)
            make.top.equalTo(self.view.snp.top).offset(barHeight + 100)
        }
        
    }
    
}
