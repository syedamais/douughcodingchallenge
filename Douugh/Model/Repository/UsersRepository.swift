//
//  UsersRepository.swift
//  Douugh
//
//  Created by SH-Syed on 25/11/18.
//  Copyright © 2018 Skilledhealth. All rights reserved.
//

import Foundation

//*****************************************************************
// USERS REPOSITORY
//*****************************************************************

class UsersRepsository : BaseService {
    
    // MARK: - Helper to get API Data
    func getUser (completion: @escaping (ItemDataResponse) -> Void ) {
        
        let url = userDataURL
        
        DispatchQueue.global(qos: .userInitiated).async {
            
            guard let usersDataURL = URL(string: url) else {
                if kDebugLog { print("User Data URL not a valid URL") }
                completion(.failure(error: "User Data URL not a valid URL"))
                return
            }
            
            super.loadDataFromURL(url: usersDataURL) { data, error in
                super.parseResult(data, completion: completion)
            }
        }
    }

}


