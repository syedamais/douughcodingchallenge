//
//  User.swift
//  Douugh
//
//  Created by SH-Syed on 25/11/18.
//  Copyright © 2018 Skilledhealth. All rights reserved.
//

import UIKit

//*****************************************************************
// USER MODEL
//*****************************************************************

struct User: Codable {
    
    var id: Int!
    var firstName: String!
    var lastName: String!
    var email: String?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case firstName = "first_name"
        case lastName = "last_name"
        case email
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .id)
        firstName = try container.decode(String.self, forKey: .firstName)
        lastName = try container.decode(String.self, forKey: .lastName)
        email = try container.decode(String.self, forKey: .email)
    }
    
}

extension User: Equatable {
    
    static func ==(lhs: User, rhs: User) -> Bool {
        return (lhs.id == rhs.id) && (lhs.firstName == rhs.firstName) && (lhs.lastName == rhs.lastName)  && (lhs.email == rhs.email)
    }
}


extension User {
    enum Comparison {
        static let sortByFirstName: (User, User) -> Bool = {
            return ($0.firstName) <
                ($1.firstName)
        }
        
        static let sortByLastName: (User, User) -> Bool = {
            return ($0.lastName) <
                ($1.lastName)
        }
        
        static let sortByID: (User, User) -> Bool = {
            return ($0.id) <
                ($1.id)
        }
        
        
    }
}


//
//extension User : Comparable {
//
//    static func < (lhs: User, rhs: User) -> Bool {
//        return (lhs.lastName, lhs.firstName) <
//            (rhs.lastName, rhs.firstName)
//    }
//}
//
//extension User {
//    enum Comparison {
//        static let firstNameSort: (User, User) -> Bool = {
//            return $0.firstName < $1.firstName
//        }
//    }
//}
