//
//  AppConstants.swift
//  Douugh
//
//  Created by SH-Syed on 25/11/18.
//  Copyright © 2018 Skilledhealth. All rights reserved.
//

import Foundation

// GENERAL SETTINGS

// Display Comments
let kDebugLog = true

// USER DATA URL
let userDataURL = "https://gist.githubusercontent.com/douughios/f3c382f543a303984c72abfc1d930af8/raw/5e6745333061fa010c64753dc7a80b3354ae324e/test-users.json"

